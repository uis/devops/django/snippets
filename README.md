# Introduction

Snippets is an applications that allows the entering of HTML snippets

## Use

Install snippets using pip:

```bash
pip install git+https://gitlab.developers.cam.ac.uk/uis/devops/django/snippets.git
```

Add snippets to the installed applications in the project configuration:

```python
    INSTALLED_APPS=(
    ...
        'snippets',
    ...
    ),
```

and add snippets in a HTML file:

```HTML
{% load snippets %}
  ...

  <head>
    ...
    {% head_snippets %}
  </head>

  <body>
    ...

    {% body_snippets %}
  </body>
```

Start the development server and visit the [Admin Page](http://127.0.0.1:8000/admin/)
to add snippets.

Visit the page you have added the snippets to to view them.
