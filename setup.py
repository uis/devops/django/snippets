from setuptools import setup, find_packages

setup(
    name='django-snippets',
    description='A Django module that allows input of HTML snippets',
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    version='1.0',
    license='MIT',
    author='DevOps Division, University Information Services, University of Cambridge',
    author_email='devops@uis.cam.ac.uk',
    packages=find_packages(),
    include_package_data=True,
    install_requires=['django>=1.11'],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
    ],

)
