from django.db import models


class Snippet(models.Model):
    """
    A HTML snippet to embed in rendered pages.

    """
    HEAD = 'head'
    BODY = 'body'
    SECTION_CHOICES = [
        (HEAD, 'head'),
        (BODY, 'body'),
    ]

    snippet = models.TextField(
        null=False, blank=False, help_text='HTML snippet')

    description = models.CharField(
        null=False, blank=False, max_length=1024,
        help_text='Human-friendly description of snippet')

    section = models.CharField(
        null=False, blank=False, max_length=32, choices=SECTION_CHOICES,
        help_text='Section of HTML this should appear in')

    is_active = models.BooleanField(default=True, help_text='Active on site')

    weight = models.IntegerField(
        null=False, blank=False, default=50,
        help_text='Weight of snippet. Higher weights appear lower in the rendered template.')

    class Meta:
        indexes = [
            # The most common query is for active snippets in different sections ordered by weight.
            models.Index(fields=['is_active', 'section', 'weight']),
        ]

    def __str__(self):
        return f'Snippet {self.id} "{self.description}"'
