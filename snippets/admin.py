from django.contrib import admin

from . import models


@admin.register(models.Snippet)
class SnippetAdmin(admin.ModelAdmin):
    list_display = ['id', 'description', 'section', 'is_active', 'weight']
    ordering = ['weight', 'id']
