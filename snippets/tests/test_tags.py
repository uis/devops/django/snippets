"""
Test for snippet template tags

"""
from django.template import Context, Template
from django.test import TestCase

from .. import models


class SnippetTests(TestCase):
    def test_basic_functionality(self):
        """
        Snippet contents appear un-escaped in rendered output

        """
        for section in ['head', 'body']:
            with self.subTest(section=section):
                snippet = models.Snippet.objects.create(
                    snippet='<script>hello;</script>', description='Test', section=section
                )

                self.assertInHTML(snippet.snippet, self._render_tag(f'{section}_snippets'))

    def test_ordered_by_weight(self):
        """
        Snippets appear in increasing weight order.

        """
        for section in ['head', 'body']:
            with self.subTest(section=section):
                for weight in [4, 3, 5, 1, 2]:
                    models.Snippet.objects.create(
                        snippet=f'{weight}', description=f'Weight {weight}', section=section,
                        weight=weight
                    )
                self.assertInHTML('12345', self._render_tag(f'{section}_snippets'))

    def test_inactive_hidden(self):
        """
        Inactive snippets do not appear

        """
        for section in ['head', 'body']:
            with self.subTest(section=section):
                models.Snippet.objects.create(
                    snippet='c', description='active', section=section, weight=30
                )
                models.Snippet.objects.create(
                    snippet='b', description='active', section=section, is_active=False,
                    weight=20
                )
                models.Snippet.objects.create(
                    snippet='a', description='active', section=section, weight=10
                )
                self.assertInHTML('ac', self._render_tag(f'{section}_snippets'))

    def _render_tag(self, tag_name):
        context = Context({})
        template = Template(f'{{% load snippets %}}{{% {tag_name} %}}')
        return template.render(context)
