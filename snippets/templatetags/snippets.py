"""
Custom template tags for HTML snippets. Enable by adding `{% load snippets %}` into your
template.

"""
from django import template
from django.utils.safestring import mark_safe

from .. import models

register = template.Library()


@register.simple_tag
def head_snippets():
    return mark_safe(get_snippet('head'))


@register.simple_tag
def body_snippets():
    return mark_safe(get_snippet('body'))


def get_snippet(section):
    snippets = (
        models.Snippet.objects
        .filter(is_active=True, section=section)
        .order_by('weight', 'id')
    )
    return ''.join(snippet.snippet for snippet in snippets)
